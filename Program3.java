//WAP to take size of array from user and also take integer elements from user Print Product of odd index olny
//Input : Enter size: 6
//Enter array Elements: 1  2  3  4  5  6 
//Output: 48.

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the Size of Array:");
		int size=sc.nextInt();

		int arr[]=new int[size];
		System.out.println("Enter the Elements of Array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}
		int prod=1;
		for(int i=0; i<arr.length; i++){

			if(i%2==1)
				prod*=arr[i];
				
		}
		System.out.println("Product of odd index is = "+ prod);
	}
}
