//WAP to take size of array from user and also take integer elements from user Print product of even elements only.

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter Elements of Array:");

		int prod=1;
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}
		int prod=1;
		for(int i=0; i<arr.length; i++){

			if( arr[i]%2==0)
				prod*=arr[i];
		}
		System.out.println("Product of even Elements is = " +prod);
	}
}
