//WAP take 7 Charcter as an input, print only ivowels from the array

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of Array:");
		int size=sc.nextInt();

		char arr[]=new char[size];
		System.out.println("Enter Elements of Array:");

		int a,e,i,o,u;
		for(int j=0; j<arr.length; j++){

			arr[j]=sc.next().charAt(0) ;
		}
		System.out.println("Output : ");
		for(int j=0; j<arr.length; j++){

			if(arr[j]=='a' || arr[j]=='e'|| arr[j]=='i'|| arr[j]=='o'|| arr[j]=='u'|| arr[j]=='A'|| arr[j]=='E'|| arr[j]=='I'|| arr[j]=='O'|| arr[j]=='U'){

				System.out.println(arr[j]+"   ");
			}
		}
	}
}
