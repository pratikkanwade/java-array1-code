// WAP take 10 input from the user and print only elements that are divisible by 5.

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		int arr[]=new int[10];
		System.out.println("Enter 10 elements :");
		
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}
		System.out.println("Elements that are divisible by 5  = ");
		for(int i=0; i<arr.length; i++){

			if(arr[i]%5==0){

				System.out.println(arr[i]);
			}
		}
	}
}
